<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $title
 * @property ?string $image
 * @property ?string $text
 * @property ?Carbon $deleted_at
 */
class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $perPage = 10;

    protected $fillable = [
        'title',
        'text',
        'image',
    ];
}
