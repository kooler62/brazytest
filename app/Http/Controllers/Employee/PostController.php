<?php

declare(strict_types=1);

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\StorePostRequest;
use App\Http\Requests\Employee\UpdatePostRequest;
use App\Services\EmployeePostService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class PostController extends Controller
{
    public function __construct(private readonly EmployeePostService $employeePostService)
    {
    }

    public function index(): View
    {
        $posts = $this->employeePostService->getPosts();

        return view('employee.posts.index', compact('posts'));
    }

    public function create(): View
    {
        return view('employee.posts.create');
    }

    public function store(StorePostRequest $request): RedirectResponse
    {
        $this->employeePostService->createPost($request->input('title'), $request->input('text'));

        return redirect()->route('admin.posts.index');
    }

    public function show(int $id): View
    {
        $post = $this->employeePostService->showPost($id);

        return view('employee.posts.show', compact('post'));
    }

    public function refreshImage(int $id): RedirectResponse
    {
        $this->employeePostService->refreshImage($id);

        return redirect()->route('admin.posts.edit', $id);
    }

    public function edit(int $id): View
    {
        $post = $this->employeePostService->showPost($id);

        return view('employee.posts.edit', compact('post'));
    }

    public function update(UpdatePostRequest $request, int $id): RedirectResponse
    {
        $this->employeePostService->updatePost($id, $request->input('title'), $request->input('text'));

        return back();
    }

    public function destroy(int $id): RedirectResponse
    {
        $this->employeePostService->deletePost($id);

        return back();
    }
}
