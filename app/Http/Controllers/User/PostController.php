<?php

declare(strict_types=1);

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\UserPostService;
use Illuminate\Contracts\View\View;

class PostController extends Controller
{
    public function __construct(private readonly UserPostService $userPostService)
    {
    }

    public function index(): View
    {
        $posts = $this->userPostService->getPosts();

        return view('user.posts.index', compact('posts'));
    }

    public function show(int $id): View
    {
        $post = $this->userPostService->showPost($id);

        return view('user.posts.show', compact('post'));
    }
}
