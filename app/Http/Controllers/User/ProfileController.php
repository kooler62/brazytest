<?php

declare(strict_types=1);

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateProfileRequest;
use App\Services\UserProfileService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ProfileController extends Controller
{
    public function __construct(private readonly UserProfileService $userProfileService)
    {
    }

    public function index(): View
    {
        $user = $this->userProfileService->getProfile();

        return view('user.profile.show', compact('user'));
    }

    public function update(UpdateProfileRequest $request, int $id): RedirectResponse
    {
        $this->userProfileService->updateProfile($request, $id);

        return back();
    }

    public function destroy(int $id): RedirectResponse
    {
        $this->userProfileService->deleteProfile();

        return redirect()->route('home');
    }
}
