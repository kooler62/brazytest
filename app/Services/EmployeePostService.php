<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Post;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class EmployeePostService
{
    public function __construct(private readonly UnsplashService $unsplashService)
    {
    }

    public function getPosts(): LengthAwarePaginator
    {
        return $this->builderWithTrashed()
            ->orderByDesc('created_at')
            ->paginate();
    }

    public function createPost(string $title, string $text): void
    {
        $this->builder()->create([
            'title' => $title,
            'text' => $text,
            'image' => $this->unsplashService->getRandomPhoto(),
        ]);
    }

    public function showPost(int $id): Model|Collection|Builder|array|null
    {
        return $this->builderWithTrashed()->findOrFail($id);
    }

    public function refreshImage(int $id): void
    {
        $post = $this->builderWithTrashed()->findOrFail($id);
        $newImageUrl = $this->unsplashService->getRandomPhoto();
        $post->update(['image' => $newImageUrl]);
    }

    public function updatePost(int $id, string $title, string $text): void
    {
        $this->builderWithTrashed()
            ->where('id', $id)
            ->update(compact('title', 'text'));
    }

    public function deletePost(int $id): void
    {
        $post = $this->builder()->findOrFail($id);
        $post->delete();
    }

    private function builder(): Builder
    {
        return Post::query();
    }

    private function builderWithTrashed(): Builder
    {
        return Post::withTrashed();
    }
}
