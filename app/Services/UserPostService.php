<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Post;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserPostService
{
    public function getPosts(): LengthAwarePaginator
    {
        return $this->builder()->orderByDesc('created_at')->paginate();
    }

    public function showPost(int $id): Model|Collection|Builder|array|null
    {
        return $this->builder()->findOrFail($id);
    }

    private function builder(): Builder
    {
        return Post::query();
    }
}
