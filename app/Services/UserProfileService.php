<?php

declare(strict_types=1);

namespace App\Services;

use App\Http\Requests\User\UpdateProfileRequest;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserProfileService
{
    public function __construct(private readonly ImageService $imageService)
    {
    }

    public function getProfile(): ?Authenticatable
    {
        return $this->authUser();
    }

    public function updateProfile(UpdateProfileRequest $request, int $id): void
    {
        /** @var User $user */
        $user = User::query()->findorFail($id);

        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ];

        if (false === is_null($request->input('password'))) {
            $data['password'] = Hash::make($request->input('password'));
        }

        $user->update($data);
        $this->imageService->updateAvatar($request, $user);
    }

    public function deleteProfile(): void
    {
        $user = $this->authUser();
        $user->delete();
    }

    private function authUser(): User
    {
        /** @var User $user */
        $user = Auth::user();

        return $user;
    }
}
