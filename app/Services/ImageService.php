<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;

class ImageService
{
    private string $folder = 'images';

    public function updateAvatar(FormRequest $request, User $user): void
    {
        if (false === is_null($user->avatar)) {
            $this->delete($user->avatar);
        }
        $this->save($request, $user);
    }

    private function delete(?string $imagePath): void
    {
        if (is_null($imagePath) === false && File::exists(public_path($imagePath))) {
            File::delete(public_path($imagePath));
        }
    }

    private function save(FormRequest $request, User $user): void
    {
        $key = 'avatar';
        if ($request->hasFile($key)) {
            $img = $request->file($key);

            $fileExtension = $img->extension();
            $generatedName = md5(time() . '_' . rand(1, 100));

            $fileName = $generatedName . ".{$fileExtension}";

            $img->storeAs($this->folder, $fileName);
            $user->{$key} = "{$this->folder}/{$fileName}";
            $user->save();
        }
    }
}
