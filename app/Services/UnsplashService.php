<?php

declare(strict_types=1);

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class UnsplashService
{
    public const URL = 'https://api.unsplash.com';

    private Client $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => self::URL]);
    }

    public function getRandomPhoto(): ?string
    {
        $clientId = config('services.unsplash.client_id');

        try {
            $response = $this->client->get("photos/random?client_id=$clientId");
            $json = json_decode((string)$response->getBody(), true);

            return $json['urls']['regular'] ?? null;
        } catch (Exception $exception) {
            Log::error($exception->getMessage());

            return null;
        }
    }
}
