<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Services\UnsplashService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     *
     * @throws BindingResolutionException
     */
    public function definition(): array
    {
        /** @var UnsplashService $unsplashService */
        $unsplashService = app()->make(UnsplashService::class);

        return [
            'title' => fake()->name(),
            'text' => fake()->text(),
            'image' => $unsplashService->getRandomPhoto(),
        ];
    }
}
