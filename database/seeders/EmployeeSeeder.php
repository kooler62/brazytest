<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    public function run(): void
    {
        if (Employee::query()->where('email', 'admin@admin.com')->first() === null) {
            Employee::query()->insert([
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin'),
            ]);
        }
    }
}
