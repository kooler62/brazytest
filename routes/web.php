<?php

declare(strict_types=1);

use App\Http\Controllers\Employee\PostController as EmployeePostController;
use App\Http\Controllers\User\PostController as UserPostController;
use App\Http\Controllers\User\ProfileController;
use Illuminate\Support\Facades\Route;

include 'auth.php';

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'auth:employee'], function () {
    Route::resource('posts', EmployeePostController::class);
    Route::get('refresh-image/{id}', [EmployeePostController::class, 'refreshImage'])->name('posts.refresh_image');
});

Route::group(['middleware' => 'auth:web'], function () {
    Route::get('/', [UserPostController::class, 'index'])->name('posts.index');
    Route::get('posts/{id}', [UserPostController::class, 'show'])->name('posts.show');
    Route::resource('profile', ProfileController::class)->only(['index', 'update', 'destroy']);
});
