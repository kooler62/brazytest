<?php

declare(strict_types=1);

use App\Http\Controllers\Auth\EmployeeLoginController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'throttle:10,30'], function () {
    Route::get('login/user', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login/user', [LoginController::class, 'login']);

    Route::get('login/admin', [EmployeeLoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('login/admin', [EmployeeLoginController::class, 'login']);
});

Route::post('logout', [LoginController::class, 'logout'])->name('logout');
Route::post('logout/employee', [EmployeeLoginController::class, 'logout'])->name('admin.logout');
