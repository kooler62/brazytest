@extends('layouts.employee_app')

@php
    use App\Models\Post;
    /** @var Post $post */
@endphp

@section('content')
    <div class="container">
        @include('components.employee.nav')

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    @forelse($posts as $post)
                        <a href="{{route('posts.show', $post->id)}}"><h3>{{$post->title}}</h3></a>
                        <img width="100" src="{{$post->image}}" alt="">

                        <a href="{{route('admin.posts.edit', $post->id)}}">edit</a>
                        @if(false === is_null($post->deleted_at))
                            <span class="text-danger">[DELETED]</span>
                        @else
                            {{ html()->form('DELETE')->route('admin.posts.destroy', $post->id)->open() }}
                            <input type="submit" class="btn-sm" value="Delete">
                            {{ html()->form()->close() }}
                        @endif
                        <hr>
                    @empty
                        empty
                    @endforelse

                    {{$posts->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
