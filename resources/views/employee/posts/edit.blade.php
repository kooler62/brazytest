@extends('layouts.employee_app')

@section('title', 'Edit Post')

@php
    use App\Models\Post;
    /** @var Post $post */
@endphp

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('components.employee.nav')

            <div class="col-md-8">
                <h1>@yield('title')</h1>

                <div class="card">
                    <img width="100" src="{{$post->image}}" alt="">
                    <a href="{{route('admin.posts.refresh_image', $post->id)}}">randomize image</a>

                    {{ html()->form('PUT')->route('admin.posts.update', $post->id)->open() }}

                    <div class='col-6 mb-3'>
                        @if ($errors->has('title'))
                            <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                        <label class="form-label" for="title">title</label>
                        {{ html()->input('text', 'title', $post->title)->id('title')->class('form-control form-control') }}
                    </div>


                    <div class='mb-3'>
                        @if ($errors->has('text'))
                            <span class="text-danger">{{ $errors->first('text') }}</span>
                        @endif
                        <label class="form-label" for="text">text</label>
                        {{ html()->textarea('text', $post->text)->id('text')->class('form-control form-control') }}
                    </div>

                    <div class='col-12 d-flex justify-content-end'>
                        <button type="submit">Update</button>
                    </div>

                    {{ html()->form()->close() }}

                </div>
            </div>
        </div>
    </div>
@endsection
