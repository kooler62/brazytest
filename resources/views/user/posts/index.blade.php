@extends('layouts.app')

@php
    use App\Models\Post;
    /** @var Post $post */
@endphp

@section('content')
    <div class="container">
        @include('components.user.nav')
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    @forelse($posts as $post)
                        <a href="{{route('posts.show', $post->id)}}"><h3>{{$post->title}}</h3></a>
                        <img width="100" src="{{$post->image}}" alt="">
                    @empty
                        empty
                    @endforelse

                    {{$posts->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
