@extends('layouts.app')

@php
    use App\Models\Post;
    /** @var Post $post */
@endphp

@section('content')
    <div class="container">
        @include('components.user.nav')
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h1>{{$post->title}}</h1>
                    <img width="100" src="{{$post->image}}" alt="">
                    <p>{{$post->text}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
