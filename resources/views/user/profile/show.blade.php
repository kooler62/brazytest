@extends('layouts.app')

@php
    use App\Models\User;
    /** @var User $user */
@endphp

@section('content')
    <div class="container">
        @include('components.user.nav')

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    {{ html()->form('PUT')->route('profile.update', $user->id)->acceptsFiles()->open() }}

                    <div class='col-6 mb-3'>
                        @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                        <label class="form-label" for="name">name</label>
                        {{ html()->input('text', 'name', $user->name)->id('name')->class('form-control form-control') }}
                    </div>

                    <div class='col-6 mb-3'>
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                        <label class="form-label" for="email">email</label>
                        {{ html()->input('email', 'email', $user->email)->id('email')->class('form-control form-control') }}
                    </div>

                    <div class='col-6 mb-3'>
                        @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                        <label class="form-label" for="password">password</label>
                        {{ html()->input('password', 'password')->id('password')->class('form-control form-control') }}
                    </div>


                    <img width="50" src="{{asset($user->avatar)}}">

                    {{ html()->input('file', 'avatar') }}
                    <button type="submit">UPDATE</button>

                    {{ html()->form()->close() }}

                    <hr>
                    {{ html()->form('DELETE')->route('profile.destroy', $user->id)->open() }}
                    <button type="submit" class="text-danger">DELETE PROFILE</button>
                    {{ html()->form()->close() }}


                </div>
            </div>
        </div>
    </div>
@endsection
